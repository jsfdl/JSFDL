package com.gitlab.jsfdl.jsfdl.mvp.download;

import com.gitlab.jsfdl.jsfdl.app.JSFDLEngine;
import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadChangedEvent;
import com.gitlab.jsfdl.jsfdl.bus.events.sfdladded.SFDLFileAddedInformation;
import com.gitlab.jsfdl.jsfdl.config.ApplicationConfig;
import com.gitlab.jsfdl.jsfdl.ftp.State;
import com.gitlab.jsfdl.jsfdl.ftp.download.SFDLFileDownloader;

import java.util.List;
import java.util.Set;

public class DownloadModel implements DownloadsMvp.Model {

    private final JSFDLEngine jsfdlEngine;

    public DownloadModel() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        jsfdlEngine = applicationConfig.getJsfdlEngine();
    }

    @Override
    public void addSfdlToDownload(SFDLFileAddedInformation fileAddedInformation) {
        jsfdlEngine.queueSFDLFile(fileAddedInformation);
    }

    @Override
    public void addDownloadChangedInformationListener(int id, DownloadsMvp.DownloadChangedListener listener) {
        jsfdlEngine.registerDownloadChangedListenerFor(id, listener);
    }

    @Override
    public Set<SFDLFileDownloader> getRunningDownloads() {
        return jsfdlEngine.getRunningDownloads();
    }

    @Override
    public List<Integer> startAllDownloads() {
        return jsfdlEngine.startAllDownloads();
    }

    @Override
    public List<Integer> abortAllDownloads() {
        return jsfdlEngine.abort();
    }

    @Override
    public void downloadChangedFor(DownloadChangedEvent downloadChangedEvent) {
        if (downloadChangedEvent.getPayload().getState().equals(State.FINISHED)) {
            Speedreporter speedy = new Speedreporter(downloadChangedEvent.getPayload());
            speedy.writeReport();
        }
    }

    @Override
    public void removeSfdl(int payload) {
        jsfdlEngine.removeSfdl(payload);
    }
}
