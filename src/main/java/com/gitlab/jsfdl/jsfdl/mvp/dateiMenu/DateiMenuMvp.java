package com.gitlab.jsfdl.jsfdl.mvp.dateiMenu;

import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLFile;

import java.net.URL;
import java.util.Optional;

public interface DateiMenuMvp {

    interface Model {

        void addedSfdl(SFDLEntry file);

        boolean sfdlAlreadyExists(SFDLEntry sfdlEntry);
    }

    interface View {

        void showSFDLSelectFileDialog();

        void registerSFDLFileRemoveListener(SFDLFileRemovedListener listener);

        void registerSFDLFileSelectedListener(SFDLFileSelectedListener listener);

        void showSFDLAddedDialog(String sfdlFile);

        Optional<String> requestPasswordDialog();

        void showWrongPasswordEntered();


    }

    interface SFDLFileSelectionDialogListener {

        void sfdlFileSelected();
    }

    interface SFDLFileSelectedListener {

        void sfdlFileSelected(URL file);
    }

    interface SFDLFileRemovedListener {

        void sfdlFileRemoved(SFDLFile file);
    }

}
