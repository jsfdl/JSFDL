/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.jsfdl.jsfdl.mvp.dateiMenu;

import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;

import java.util.HashSet;
import java.util.Set;

/**
 * @author oli
 */
public class DateiMenuModel implements DateiMenuMvp.Model {

    private final Set<SFDLEntry> files = new HashSet<>();

    @Override
    public void addedSfdl(SFDLEntry file) {
        files.add(file);
    }

    @Override
    public boolean sfdlAlreadyExists(SFDLEntry sfdlEntry) {
        return files.contains(sfdlEntry);
    }

}
