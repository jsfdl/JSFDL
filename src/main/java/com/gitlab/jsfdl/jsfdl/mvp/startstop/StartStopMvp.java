package com.gitlab.jsfdl.jsfdl.mvp.startstop;

public interface StartStopMvp {

    interface Model {

    }

    interface View {

        void registerStartDownloadListener(StartDownloadListener actionListener);

        void registerCancelDownloadListener(CancelDownloadListener actionListener);

        void viewDownloadsAreStopped();

        void viewDownloadsAreStarted();

        void showNothingToAbort();

        void viewNoDownloadsToDownloads();

    }

    interface StartDownloadListener {

        void downloadsStarted();
    }

    interface CancelDownloadListener {

        void downloadsCanceled();
    }

}
