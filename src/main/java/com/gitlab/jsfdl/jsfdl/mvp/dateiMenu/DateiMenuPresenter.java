package com.gitlab.jsfdl.jsfdl.mvp.dateiMenu;

import com.gitlab.jsfdl.jsfdl.bus.AbstractMessagePresenter;
import com.gitlab.jsfdl.jsfdl.bus.Event;
import com.gitlab.jsfdl.jsfdl.bus.EventAdmin;
import com.gitlab.jsfdl.jsfdl.bus.EventWithPayload;
import com.gitlab.jsfdl.jsfdl.bus.events.sfdladded.SFDLFileAdded;
import com.gitlab.jsfdl.jsfdl.bus.events.sfdladded.SFDLFileAddedInformation;
import com.gitlab.jsfdl.jsfdl.bus.events.sfdlremoved.SFDLFileRemoved;
import com.gitlab.jsfdl.jsfdl.config.ApplicationConfig;
import com.gitlab.jsfdl.jsfdl.sfdl.SFDLCryptor;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLFile;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLFileFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class DateiMenuPresenter extends AbstractMessagePresenter {

    private final DateiMenuMvp.Model model;
    private final DateiMenuMvp.View view;
    private final EventAdmin eventAdmin = EventAdmin.getInstance();

    public DateiMenuPresenter(DateiMenuMvp.Model model, DateiMenuMvp.View view) {
        this.model = model;
        this.view = view;

        view.registerSFDLFileSelectedListener(this::sfdlFileSelected);
        view.registerSFDLFileRemoveListener(this::sfdlFileRemoved);
    }

    private void sfdlFileRemoved(SFDLFile sfdlFile) {
        eventAdmin.fireEvent(new SFDLFileRemoved(sfdlFile.getId()));
    }

    /**
     * The dialog has been shown and the user selected the sfdlfile
     *
     * @param sfdlFileUrl the sfdl which has been selected by the user
     */
    private void sfdlFileSelected(URL sfdlFileUrl) {

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[16384];
        InputStream is;
        try {
            is = sfdlFileUrl.openStream();
            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();
            is.close();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        //TODO: move that into constructor/application config!
        SFDLFileFactory factory = new SFDLFileFactory(new ApplicationConfig().getSFDLBeanToSFDLFileConverter());
        SFDLFile sfdlFile = factory.getSfdlFile(buffer.toByteArray());

        while (sfdlFile.isEncrypted()) {

            final Optional<String> password = view.requestPasswordDialog();
            if (!password.isPresent()) {
                return;
            }
            //the dialog must be a modal one, so we continue here without listener
            //as we expect it is finished and a password has been entered.
            //TODO: abort button or such would be nice!

            try {
                sfdlFile.decrypt(password.get());
            } catch (SFDLCryptor.DecryptionException ex) {
                view.showWrongPasswordEntered();
            }

        }

        final List<SFDLEntry> sfdlFileEntries = sfdlFile.getEntries();
        for (SFDLEntry sfdlEntry : sfdlFileEntries) {
            if (model.sfdlAlreadyExists(sfdlEntry))
                continue;
            model.addedSfdl(sfdlEntry);
//        view.showSFDLAddedDialog(sfdlFile.getFilePath());
            eventAdmin.fireEvent(new SFDLFileAdded(new SFDLFileAddedInformation(sfdlEntry, sfdlFileUrl.getFile())));
        }

    }

    private void sfdlPasswordEntered() {

    }

    @Override
    public void eventFired(EventWithPayload eventWithPayload) {

    }

    @Override
    public Set<Class> eventsToListenFor() {
        return Collections.emptySet();
    }

    @Override
    public void eventFired(Event event) {

    }
}
