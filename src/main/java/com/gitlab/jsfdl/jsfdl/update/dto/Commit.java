package com.gitlab.jsfdl.jsfdl.update.dto;

import java.util.Date;
import java.util.List;

public class Commit {

    private String id;
    private String message;
    private List<String> parent_ids;
    private Date authored_date;
    private String author_name;
    private String author_email;
    private Date commited_date;
    private String commiter_name;
    private String commiter_email;

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public List<String> getParent_ids() {
        return parent_ids;
    }

    public Date getAuthored_date() {
        return (Date) authored_date.clone();
    }

    public String getAuthor_name() {
        return author_name;
    }

    public String getAuthor_email() {
        return author_email;
    }

    public Date getCommited_date() {
        return (Date) commited_date.clone();
    }

    public String getCommiter_name() {
        return commiter_name;
    }

    public String getCommiter_email() {
        return commiter_email;
    }
}
