package com.gitlab.jsfdl.jsfdl.update.dto;

public class Tag {

    private String name;
    private String message;
    private Commit commit;
    private Release release;

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    public Commit getCommit() {
        return commit;
    }

    public Release getRelease() {
        return release;
    }

    public int getTagAsInt() {

        return Integer.parseInt(name.split("v")[1].replace(".", ""));

    }

}
