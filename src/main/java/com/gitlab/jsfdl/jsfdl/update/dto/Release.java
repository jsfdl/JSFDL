package com.gitlab.jsfdl.jsfdl.update.dto;

public class Release {

    private String tag_name;
    private String description;

    public String getTag_name() {
        return tag_name;
    }

    public String getDescription() {
        return description;
    }



}
