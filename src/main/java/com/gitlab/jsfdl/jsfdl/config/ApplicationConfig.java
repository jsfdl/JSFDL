package com.gitlab.jsfdl.jsfdl.config;

import com.gitlab.jsfdl.jsfdl.app.JSFDLConfig;
import com.gitlab.jsfdl.jsfdl.app.JSFDLEngine;
import com.gitlab.jsfdl.jsfdl.ftp.client.FTPFactory;
import com.gitlab.jsfdl.jsfdl.ftp.client.MyFTPFactory;
import com.gitlab.jsfdl.jsfdl.ftp.listmode.FTPValidator;
import com.gitlab.jsfdl.jsfdl.sfdl.model.ConnectionInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.model.FileInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.model.Packages;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.converter.ConnectionInfoConverter;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.converter.FileInfoConverter;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.converter.PackagesConverter;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.converter.SFDLBeanToSFDLFileConverter;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlConnectionInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlFileInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlPackages;
import com.gitlab.jsfdl.jsfdl.shared.Converter;
import com.gitlab.jsfdl.jsfdl.update.VersionFetcher;
import com.gitlab.jsfdl.jsfdl.update.VersionInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationConfig {

    private final static int projectId = 1615127; //JSFDLSWING: 1620843
    private final Logger logger = LogManager.getLogger();

    public JSFDLEngine getJsfdlEngine() {
        return JSFDLEngine.getInstance(getJSFDLConfig());
    }

    public JSFDLConfig getJSFDLConfig() {
        return new JSFDLConfig("Downloads", "sfdls");
    }

    public VersionInfo getVersionInfo() {
        return new VersionInfo(projectId, getCurrentVersion(), getVersionFetcher());
    }

    public VersionFetcher getVersionFetcher() {
        return new VersionFetcher();
    }

    public int getCurrentVersion() {
        final InputStream resourceAsStream = getClass().getResourceAsStream("version.properties");
        Properties properties = new Properties();
        try {
            properties.load(resourceAsStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                resourceAsStream.close();
            } catch (IOException e) {

            }
        }
        return Integer.parseInt(properties.getProperty("version").replaceAll("\\.", ""));
    }

    public FTPFactory getFTPFactory() {
        return new MyFTPFactory();
    }

    public FTPValidator getFtpValidator() {
        return FTPValidator.getInstance((getFTPFactory().createFTPClient()));
    }

    public SFDLBeanToSFDLFileConverter getSFDLBeanToSFDLFileConverter() {
        return new SFDLBeanToSFDLFileConverter(getConnectionInfoConverter(), getPackagesConverter());
    }

    public Converter<XmlConnectionInfo, ConnectionInfo> getConnectionInfoConverter() {
        return new ConnectionInfoConverter();
    }

    public Converter<XmlPackages, Packages> getPackagesConverter() {
        return new PackagesConverter(getFileInfoConverter());
    }

    public Converter<XmlFileInfo, FileInfo> getFileInfoConverter() {
        return new FileInfoConverter();
    }
}
