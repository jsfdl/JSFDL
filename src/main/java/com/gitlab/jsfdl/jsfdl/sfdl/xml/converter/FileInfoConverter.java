package com.gitlab.jsfdl.jsfdl.sfdl.xml.converter;

import com.gitlab.jsfdl.jsfdl.sfdl.model.FileInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlFileInfo;
import com.gitlab.jsfdl.jsfdl.shared.Converter;

public class FileInfoConverter implements Converter<XmlFileInfo, FileInfo> {
    @Override
    public FileInfo convert(XmlFileInfo xmlFileInfo) {
        FileInfo fileInfo1 = new FileInfo();
        fileInfo1.setFileName(xmlFileInfo.getFileName());
        fileInfo1.setDirectoryPath(xmlFileInfo.getDirectoryPath());
        fileInfo1.setDirectoryRoot(xmlFileInfo.getDirectoryRoot());
        fileInfo1.setFileFullPath(xmlFileInfo.getFileFullPath());
        fileInfo1.setFileHash(xmlFileInfo.getFileHash());
        fileInfo1.setFileHashType(xmlFileInfo.getFileHashType());
        fileInfo1.setPackageName(xmlFileInfo.getPackageName());
        fileInfo1.setFileSize(xmlFileInfo.getFileSize());
        return fileInfo1;
    }
}
