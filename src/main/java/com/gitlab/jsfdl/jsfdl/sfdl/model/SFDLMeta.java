package com.gitlab.jsfdl.jsfdl.sfdl.model;

import com.gitlab.jsfdl.jsfdl.sfdl.SFDLCryptor;

public class SFDLMeta {

    private String description;
    private String uploader;
    private final Integer sfdlFileVersion;
    private final int maxThreads;
    private final boolean sfdlEncrypted;

    public SFDLMeta(String description, String uploader, Integer sfdlFileVersion, int maxThreads, boolean sfdlEncrypted) {
        this.description = description;
        this.uploader = uploader;
        this.sfdlFileVersion = sfdlFileVersion;
        this.maxThreads = maxThreads;
        this.sfdlEncrypted = sfdlEncrypted;
    }

    public String getDescription() {
        return description;
    }

    public String getUploader() {
        return uploader;
    }

    public Integer getSfdlFileVersion() {
        return sfdlFileVersion;
    }

    public int getMaxThreads() {
        return maxThreads;
    }

    public boolean isSfdlEncrypted() {
        return sfdlEncrypted;
    }

    public void decrypt(String password) {
        this.description = SFDLCryptor.decrypt(password, this.description);
        this.uploader = SFDLCryptor.decrypt(password, this.uploader);
    }
}
