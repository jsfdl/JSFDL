package com.gitlab.jsfdl.jsfdl.sfdl.xml.converter;

import com.gitlab.jsfdl.jsfdl.sfdl.model.ConnectionInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlConnectionInfo;
import com.gitlab.jsfdl.jsfdl.shared.Converter;

public class ConnectionInfoConverter implements Converter<XmlConnectionInfo, ConnectionInfo> {
    @Override
    public ConnectionInfo convert(XmlConnectionInfo xmlConnectionInfo) {
        return new ConnectionInfo(xmlConnectionInfo.getHost(), xmlConnectionInfo.getPort(), xmlConnectionInfo.getUsername(), xmlConnectionInfo.getPassword());
    }
}
