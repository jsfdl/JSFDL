package com.gitlab.jsfdl.jsfdl.sfdl.xml.converter;

import com.gitlab.jsfdl.jsfdl.sfdl.model.ConnectionInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.model.Packages;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLFile;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLMeta;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.SFDLBean;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlConnectionInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlPackages;
import com.gitlab.jsfdl.jsfdl.shared.Converter;

public class SFDLBeanToSFDLFileConverter implements Converter<SFDLBean, SFDLFile> {

    private final Converter<XmlConnectionInfo, ConnectionInfo> connectionInfoConverter;
    private final Converter<XmlPackages, Packages> packagesConverter;

    public SFDLBeanToSFDLFileConverter(Converter<XmlConnectionInfo, ConnectionInfo> connectionInfoConverter,
                                       Converter<XmlPackages, Packages> packagesConverter) {
        this.connectionInfoConverter = connectionInfoConverter;
        this.packagesConverter = packagesConverter;
    }

    @Override
    public SFDLFile convert(SFDLBean sfdlBean) {
        final ConnectionInfo connectionInfo = connectionInfoConverter.convert(sfdlBean.getXmlConnectionInfo());
        final String description = sfdlBean.getDescription();
        final int maxDownloadThreads = sfdlBean.getMaxDownloadThreads();
        Packages packages = packagesConverter.convert(sfdlBean.getXmlPackages());
        final int sfdlFileVersion = sfdlBean.getSfdlFileVersion();
        final String uploader = sfdlBean.getUploader();
        final boolean encrypted = sfdlBean.isEncrypted();
        final SFDLMeta sfdlMeta = new SFDLMeta(description, uploader, sfdlFileVersion, maxDownloadThreads, encrypted);

        return new SFDLFile(packages, sfdlMeta, connectionInfo);
    }
}


