package com.gitlab.jsfdl.jsfdl.sfdl.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SFDLFile {

    private final Packages packages;
    private final SFDLMeta sfdlMeta;
    private final ConnectionInfo connectionInfo;
    private boolean isEncrypted;
    private long packageSize = -1;

    public SFDLFile(Packages packages, SFDLMeta sfdlMeta,  ConnectionInfo connectionInfo) {
        this.packages = packages;
        this.sfdlMeta = sfdlMeta;
        this.connectionInfo = connectionInfo;
        isEncrypted = sfdlMeta.isSfdlEncrypted();
    }

    public String getDescription() {
        return sfdlMeta.getDescription();
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public ConnectionInfo getConnectionInfo() {
        return connectionInfo;
    }

    public int getMaxThreads() {
        return sfdlMeta.getMaxThreads();
    }

    public int getId() {
        return hashCode();
    }

    public void decrypt(String password) {
        if (!isEncrypted) {
            throw new RuntimeException("Not encrypted!");
        }

        sfdlMeta.decrypt(password);
        connectionInfo.decrypt(password);
        packages.decrypt(password);
        isEncrypted = false;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SFDLFile sfdlFile = (SFDLFile) o;
        return packageSize == sfdlFile.packageSize &&
                Objects.equals(packages, sfdlFile.packages) &&
                Objects.equals(sfdlMeta, sfdlFile.sfdlMeta) &&
                Objects.equals(connectionInfo, sfdlFile.connectionInfo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(packages, sfdlMeta, connectionInfo, packageSize);
    }


    public List<SFDLEntry> getEntries() {
        final List<SFDLEntry> entries = new ArrayList<>();
        for (SFDLPackage sfdlPackage : packages.getSfdlPackages()) {
            SFDLEntry sfdlEntry = new SFDLEntry(sfdlMeta, sfdlPackage, connectionInfo);
            entries.add(sfdlEntry);
        }
        return entries;
    }

    @Override
    public String toString() {
        return "SFDLFile{" +
                "packages=" + packages +
                ", sfdlMeta=" + sfdlMeta +
                ", connectionInfo=" + connectionInfo +
                ", packageSize=" + packageSize +
                '}';
    }

    public int getPackagesCount() {
        return packages.count();
    }

    public SFDLMeta getMeta() {
        return sfdlMeta;
    }
}
