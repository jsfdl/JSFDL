package com.gitlab.jsfdl.jsfdl.sfdl;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public final class SFDLCryptor {

    public static String decrypt(String password, String encodedString) {

        if (encodedString == null || encodedString.isEmpty())
            return encodedString;

        try {

            byte[] data = encodedString.getBytes(StandardCharsets.UTF_8);

            MessageDigest md5pass = MessageDigest.getInstance("MD5");
            md5pass.reset();
            byte[] pass = md5pass.digest(password.getBytes(StandardCharsets.UTF_8));

            IvParameterSpec iv = new IvParameterSpec(Arrays.copyOfRange(data, 0, 16));
            SecretKeySpec keyspec = new SecretKeySpec(pass, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, keyspec, iv);

            byte[] decrypted = cipher.doFinal(java.util.Base64.getDecoder().decode(data));

            return new String(Arrays.copyOfRange(decrypted, 16, decrypted.length));
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException ex) {
            throw new DecryptionException(ex.getLocalizedMessage());
        }
    }

    public static class DecryptionException extends RuntimeException {

        public DecryptionException(String message) {
            super(message);
        }
    }
}
