package com.gitlab.jsfdl.jsfdl.sfdl.xml.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * DTO for XML marshalling/unmarshalling
 */
@XmlRootElement(name = "SFDLFile")
public final class SFDLBean {

    @XmlElement(name = "Description")
    private String description;

    @XmlElement(name = "Uploader")
    private String uploader;

    @XmlElement(name = "SFDLFileVersion")
    private Integer sfdlFileVersion;

    @XmlElement(name = "Encrypted")
    private boolean encrypted;

    @XmlElement(name = "ConnectionInfo")
    private XmlConnectionInfo xmlConnectionInfo;

    @XmlElement(name = "Packages")
    private XmlPackages xmlPackages;

    @XmlElement(name = "MaxDownloadThreads")
    private int maxDownloadThreads;

//    public void decrypt(String password) {
//        if (!encrypted) {
//            throw new RuntimeException("Not encrypted!");
//        }
//
//        this.description = SFDLCryptor.decrypt(password, this.description);
//        this.uploader = SFDLCryptor.decrypt(password, this.uploader);
//
//        connectionInfo.decrypt(password);
//        packages.decrypt(password);
//
//        this.encrypted = false;
//
//    }

    public String getDescription() {
        return description;
    }

    public String getUploader() {
        return uploader;
    }

    public Integer getSfdlFileVersion() {
        return sfdlFileVersion;
    }

    public boolean isEncrypted() {
        return encrypted;
    }

    public XmlConnectionInfo getXmlConnectionInfo() {
        return xmlConnectionInfo;
    }

    public XmlPackages getXmlPackages() {
        return xmlPackages;
    }

    public int getMaxDownloadThreads() {
        return maxDownloadThreads;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SFDLBean sfdlBean = (SFDLBean) o;

        if (encrypted != sfdlBean.encrypted) return false;
        if (maxDownloadThreads != sfdlBean.maxDownloadThreads) return false;
        if (description != null ? !description.equals(sfdlBean.description) : sfdlBean.description != null)
            return false;
        if (uploader != null ? !uploader.equals(sfdlBean.uploader) : sfdlBean.uploader != null) return false;
        if (sfdlFileVersion != null ? !sfdlFileVersion.equals(sfdlBean.sfdlFileVersion) : sfdlBean.sfdlFileVersion != null)
            return false;
        if (xmlConnectionInfo != null ? !xmlConnectionInfo.equals(sfdlBean.xmlConnectionInfo) : sfdlBean.xmlConnectionInfo != null)
            return false;
        return xmlPackages != null ? xmlPackages.equals(sfdlBean.xmlPackages) : sfdlBean.xmlPackages == null;
    }

    @Override
    public int hashCode() {
        int result = description != null ? description.hashCode() : 0;
        result = 31 * result + (uploader != null ? uploader.hashCode() : 0);
        result = 31 * result + (sfdlFileVersion != null ? sfdlFileVersion.hashCode() : 0);
        result = 31 * result + (encrypted ? 1 : 0);
        result = 31 * result + (xmlConnectionInfo != null ? xmlConnectionInfo.hashCode() : 0);
        result = 31 * result + (xmlPackages != null ? xmlPackages.hashCode() : 0);
        result = 31 * result + maxDownloadThreads;
        return result;
    }
}
