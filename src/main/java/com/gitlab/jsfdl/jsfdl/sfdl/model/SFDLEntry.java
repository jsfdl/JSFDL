package com.gitlab.jsfdl.jsfdl.sfdl.model;

import com.gitlab.jsfdl.jsfdl.ftp.bulkmode.PackageName;

import java.util.List;

public class SFDLEntry {

    private final SFDLMeta sfdlMeta;
    private final SFDLPackage sfdlPackage;
    private final ConnectionInfo connectionInfo;

    public SFDLEntry(SFDLMeta sfdlMeta, SFDLPackage sfdlPackage, ConnectionInfo connectionInfo) {
        this.sfdlMeta = sfdlMeta;
        this.sfdlPackage = sfdlPackage;
        this.connectionInfo = connectionInfo;
    }

    public boolean isBulkfolder() {
        return sfdlPackage.isBulkFolderMode();
    }

    public ConnectionInfo connectionInfo() {
        return connectionInfo;
    }

    public String remoteSourceDir() {
        return sfdlPackage.getRemoteSourceDir();
    }

    public List<FileInfo> getDefinedFiles() {
        return sfdlPackage.getDefinedFiles();
    }

    public int getMaxThreads() {
        return sfdlMeta.getMaxThreads();
    }

    public void decrypt(String password) {
        sfdlMeta.decrypt(password);
    }

    public int getId() {
        return hashCode();
    }

    public String getDisplayName() {
        if (!sfdlMeta.getDescription().trim().isEmpty()) {
            return sfdlMeta.getDescription();
        }

        if (!sfdlPackage.getPackageName().trim().isEmpty()) {
            return sfdlPackage.getPackageName();
        }

        if (sfdlPackage.isBulkFolderMode()) {
            return new PackageName(sfdlPackage.getRemoteSourceDir()).getPackagenameFromSourcedir();
        }

        return "unable to find the name :/";
    }

    public String getBulkRemoteSourceDir() {
        return sfdlPackage.getBulkFolderPath();
    }

    public String getPackageName() {
        return sfdlPackage.getPackageName();
    }

    @Override
    public String toString() {
        return "SFDLFile{" +
                "sfdlFileDefinition=" + sfdlMeta +
                '}';
    }

    public List<FileInfo> getFileInfos() {
        return sfdlPackage.getFileInfos();
    }

}
