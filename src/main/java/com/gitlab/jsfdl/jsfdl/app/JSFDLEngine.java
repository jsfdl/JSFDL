package com.gitlab.jsfdl.jsfdl.app;

import com.gitlab.jsfdl.jsfdl.bus.events.sfdladded.SFDLFileAddedInformation;
import com.gitlab.jsfdl.jsfdl.config.ApplicationConfig;
import com.gitlab.jsfdl.jsfdl.ftp.State;
import com.gitlab.jsfdl.jsfdl.ftp.download.SFDLDownloaderThreadPoolExecutor;
import com.gitlab.jsfdl.jsfdl.ftp.download.SFDLFileDownloader;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class JSFDLEngine {

    private static JSFDLEngine instance;
    private final JSFDLConfig config;
    private SFDLDownloaderThreadPoolExecutor downloaderThreadPool;
    private ApplicationConfig applicationConfig = new ApplicationConfig();

    private JSFDLEngine(JSFDLConfig jsfdlConfig) {
        //at the moment, we allow global 3 downloads, maybe i should change that?
        //this has nothing to do with the per-sfdl-max-threads ! this is the global threadcount! 
        downloaderThreadPool = new SFDLDownloaderThreadPoolExecutor(50);
        config = jsfdlConfig;
    }

    public static JSFDLEngine getInstance(JSFDLConfig config) {
        synchronized (JSFDLEngine.class) {
            if (instance == null) {
                instance = new JSFDLEngine(config);
            }
        }
        return instance;
    }

    public void queueSFDLFile(SFDLFileAddedInformation downloadInformation) {
        final SFDLEntry sfdlEntry = downloadInformation.getSfdlEntry();
        final SFDLFileDownloader sfdlFileDownloader = new SFDLFileDownloader(sfdlEntry, config, applicationConfig.getFtpValidator(), applicationConfig.getFTPFactory());
        downloaderThreadPool.register(sfdlFileDownloader);
    }

    public void registerDownloadChangedListenerFor(int id, DownloadsMvp.DownloadChangedListener listener) {
        Optional<SFDLFileDownloader> findFirst = downloaderThreadPool.getRegisteredTasks().stream().filter((t) -> t.getSfdlEntry().getId() == id).findFirst();
        if (findFirst.isPresent()) {
            findFirst.get().addDownloadChangedListener(listener);
        }
    }

    /**
     * Aborts all downloads which are running now and replaces the current
     * download ThreadPool(!).
     *
     * @return a list of IDs of downloads which where aborted.
     */
    public List<Integer> abort() {
        final List<Integer> aborted = new ArrayList<>();
        downloaderThreadPool.shutdownNow();
        Set<SFDLFileDownloader> registeredTasks = downloaderThreadPool.getRegisteredTasks();
        registeredTasks.stream().filter((t) -> {
            return t.getState().equals(State.DOWNLOADING);
        }).forEach((t) -> aborted.add(t.getSfdlEntry().getId()));
        registeredTasks.stream().filter((t) -> {
            return t.getState().equals(State.DOWNLOADING);
        }).forEach((t) -> t.abortAllDownloads());
        downloaderThreadPool = new SFDLDownloaderThreadPoolExecutor(50);
        downloaderThreadPool.addRegisteredTasks(registeredTasks);
        return aborted;
    }

    public Set<SFDLFileDownloader> getRunningDownloads() {
        return downloaderThreadPool.getRunningDownloads();
    }

    public List<Integer>  startAllDownloads() {
        return downloaderThreadPool.startAllDownloads();
    }

    public void removeSfdl(int payload) {
        downloaderThreadPool.removeSfdl(payload);
    }

}
