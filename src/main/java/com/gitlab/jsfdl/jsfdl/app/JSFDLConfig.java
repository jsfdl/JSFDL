package com.gitlab.jsfdl.jsfdl.app;

import com.gitlab.jsfdl.jsfdl.ftp.download.Blacklister;

/**
 * Configuration class which represents the command line arguments.
 */
public class JSFDLConfig {

    private final String destinationDir;
    private final String sfdlDirectory;
    private Blacklister blacklister = new Blacklister();

    public JSFDLConfig(String destinationDir, String sfdlDirectory) {
        this.destinationDir = destinationDir;
        this.sfdlDirectory = sfdlDirectory;
    }

//    public JSFDLConfig(String des) {
//
//      //TODO: this is wrong here, move that a class upper where the params are given, probably main()
//        final OptionParser optionParser = new OptionParser();
//        optionParser.accepts("d", "Destination directory for storing the downloads.").withRequiredArg();
//        optionParser.accepts("s", "Source directory where the sfdls are located.").withRequiredArg();
//        final OptionSet options = optionParser.parse(args);
//
//        if (options.hasArgument("d")) {
//            destinationDir = (String) options.valueOf("d");
//        } else {
//            destinationDir = "downloads";
//        }
//
//        if (options.hasArgument("s")) {
//            sfdlDirectory = (String) options.valueOf("s");
//        } else {
//            sfdlDirectory = "sfdls";
//        }
//    }
    public String getDestinationDir() {
        return destinationDir;
    }

    public String getSfdlDirectory() {
        return sfdlDirectory;
    }

    public Blacklister getBlacklister() {
        return blacklister;
    }
}
