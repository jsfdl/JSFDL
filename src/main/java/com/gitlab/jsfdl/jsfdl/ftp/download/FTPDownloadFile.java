package com.gitlab.jsfdl.jsfdl.ftp.download;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FTPDownloadFile {
    private final Logger logger = LogManager.getLogger();
    private final String packageName;
    private final String remoteAbsoluteFileLocationOnServer;
    private final String sourceDirectoryOnServer;
    private final long size;

    public FTPDownloadFile(String packageName, String sourceDirectoryOnServer, String remoteAbsoluteFileLocationOnServer, long size) {
        this.packageName = packageName;
        this.sourceDirectoryOnServer = sourceDirectoryOnServer;
        this.remoteAbsoluteFileLocationOnServer = remoteAbsoluteFileLocationOnServer;
        this.size = size;
    }

    public String getRemoteAbsoluteFileLocation() {
        return remoteAbsoluteFileLocationOnServer;
    }

    public String getSourceDirectoryOnServer() {
        return sourceDirectoryOnServer;
    }

    public String getPackageName() {
        return packageName;
    }

    long getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "FTPDownloadFile{" +
                "packageName='" + packageName + '\'' +
                ", remoteAbsoluteFileLocationOnServer='" + remoteAbsoluteFileLocationOnServer + '\'' +
                ", sourceDirectoryOnServer='" + sourceDirectoryOnServer + '\'' +
                ", size=" + size +
                '}';
    }
}
