package com.gitlab.jsfdl.jsfdl.ftp.download;

import java.io.File;

public interface Saveable {

    void to(File destination);

}
