package com.gitlab.jsfdl.jsfdl.ftp.client;

public class FileNotAvailableOnFtp extends Exception {
    public FileNotAvailableOnFtp(String file) {
        super("Unable to access file: " + file);
    }
}
