package com.gitlab.jsfdl.jsfdl.ftp;

public interface DetailTransferListener {

    void transfered(String subfile, long bytesTransfered, long bytesForCompleteFile);

    void finished(String subfile, long bytesInTotal);
}
