package com.gitlab.jsfdl.jsfdl.ftp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class TimeWatch {
    private final Logger logger = LogManager.getLogger();
    private long starts;

    private TimeWatch() {
        reset();
    }

    public static TimeWatch start() {
        return new TimeWatch();
    }

    public void reset() {
        logger.trace(() -> "watch resetted!");
        starts = System.nanoTime();
    }

    public long time() {
        long ends = System.nanoTime();
        return ends - starts;
    }

    public boolean oneSecondPassed() {
        return TimeUnit.SECONDS.convert(time(), TimeUnit.NANOSECONDS) >= 1;
    }
}