package com.gitlab.jsfdl.jsfdl.ftp.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyFTPFactory implements FTPFactory {
    private final static Logger logger = LogManager.getLogger();

    @Override
    public MyFTPClient createFTPClient() {
        final MyFTPClient ftpDownloadClient;
        ftpDownloadClient = new MyFTPClient();
        ftpDownloadClient.setBufferSize(1024 * 1024);
        return ftpDownloadClient;
    }

}
