/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.jsfdl.jsfdl.ftp.listmode;

import com.gitlab.jsfdl.jsfdl.ftp.client.MyFTPClient;
import com.gitlab.jsfdl.jsfdl.sfdl.model.ConnectionInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;
import org.apache.commons.net.ftp.FTP;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.commons.net.ftp.FTPClient.FTP_SYSTEM_TYPE;

/**
 * Validates the files in listmode. We get them from the .xml so we make sure
 * the files exists, are readable and the server responds correctly.
 *
 * @author oli
 */
public class FTPValidator {

    private final static int SOCKET_TIMEOUT_FOR_CONNECT = 10000;
    private final MyFTPClient traversalFtpClient;

    private final static Map<MyFTPClient, FTPValidator> instances = new HashMap<>();

    public List<VALIDATION_STATE> validate(SFDLEntry sfdlEntry) {
        final List<VALIDATION_STATE> errors = new ArrayList<>();
        traversalFtpClient.setConnectTimeout(SOCKET_TIMEOUT_FOR_CONNECT);
        final ConnectionInfo connectionInfoV9 = sfdlEntry.connectionInfo();

        try {
            traversalFtpClient.connect(connectionInfoV9.getHost(), connectionInfoV9.getPort());

            if (connectionInfoV9.getUsername().isEmpty()) {
                traversalFtpClient.login("anonymous", "");
            } else {
                traversalFtpClient.login(connectionInfoV9.getUsername(), connectionInfoV9.getPassword());
            }

            traversalFtpClient.enterLocalPassiveMode();
            traversalFtpClient.setListHiddenFiles(true);
            traversalFtpClient.setFileType(FTP.BINARY_FILE_TYPE);
            String sourceDir = sfdlEntry.remoteSourceDir();
            boolean fileToDownloadExists = traversalFtpClient.changeWorkingDirectory(sourceDir);
            if (!fileToDownloadExists) {
                errors.add(VALIDATION_STATE.FILES_OFFLINE);
            }
            try {
                traversalFtpClient.listFiles();
            } catch (Exception ex) {
                //crapshit implementation of ftp. if the system type is not properly set in the ftp server
                //we will get serious problems retrieving FTPFiles. Therefore, if we encounter such a crappy
                //ftp server, we will try to use "UNIX" as default. Maybe i should write some kind of trial-and-error
                //to get the correct system type.
                System.setProperty(FTP_SYSTEM_TYPE, "UNIX");
            }
        } catch (IOException ex) {
            errors.add(VALIDATION_STATE.SERVER_OFFLINE);
        }
//            filesToDownload.addAll(collectFiles(traversalFtpClient, sourceDir)); //TODO, at the moment its enough to check if the dir is there or not
        return errors;
    }

    private FTPValidator(MyFTPClient traversalFtpClient) {
        this.traversalFtpClient = traversalFtpClient;
    }

    public static FTPValidator getInstance(MyFTPClient ftpClient) {
        if (instances.containsKey(ftpClient)) {
            return instances.get(ftpClient);
        }

        final FTPValidator ftpValidator = new FTPValidator(ftpClient);
        instances.put(ftpClient, ftpValidator);
        return ftpValidator;
    }

    public enum VALIDATION_STATE {
        SERVER_OFFLINE, AUTHENTICATION_ERROR, FILE_MISSING, FILE_CORRUPT, FILES_OFFLINE
    }
}
