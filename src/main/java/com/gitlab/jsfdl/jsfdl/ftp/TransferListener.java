package com.gitlab.jsfdl.jsfdl.ftp;

public interface TransferListener {

    /**
     * Informs listeners of the state of the SFDLFile
     *
     * @param bytes The overall filesize which was transfered
     */
    void transfered(long bytes);

    /**
     * If the complete package is finished including all download threads, this
     * method is fired.
     *
     * @param packageName The package name of the SFDL File
     */
    void downloadFinished(String packageName);

    void transfering(boolean transfering);
}
