package com.gitlab.jsfdl.jsfdl.ftp.bulkmode;

public class PackageName {

    private final String sourceDir;

    public PackageName(String sourceDir) {
        this.sourceDir = sourceDir;
    }

    public String getPackagenameFromSourcedir() {

        boolean removeFirstSlash = false;
        boolean removeLastSlah = false;

        if (sourceDir.startsWith("/")) {
            removeFirstSlash = true;
        }

        if (sourceDir.endsWith("/")) {
            removeLastSlah = true;
        }

        StringBuilder buffer = new StringBuilder(sourceDir);
        if (removeFirstSlash)
            buffer.deleteCharAt(0);
        if (removeLastSlah)
            buffer.deleteCharAt(buffer.length() - 1);

        if (buffer.indexOf("/") == -1) {
            return buffer.toString();
        } else {
            return buffer.substring(buffer.indexOf("/") + 1, buffer.length());
        }
    }

}
