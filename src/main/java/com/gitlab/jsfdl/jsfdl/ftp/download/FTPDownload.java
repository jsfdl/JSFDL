/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.jsfdl.jsfdl.ftp.download;

import java.util.ArrayList;
import java.util.List;

/**
 * @author oli
 */
public class FTPDownload {

    private final List<FTPDownloadFile> downloadFiles = new ArrayList();

    void addFile(FTPDownloadFile ftpDownloadFile) {
        downloadFiles.add(ftpDownloadFile);
    }

    List<FTPDownloadFile> getDownloadFiles() {
        return downloadFiles;
    }

    long getPackageSize() {
        long size = 0;
        size = downloadFiles.stream().map((file) -> file.getSize()).reduce(size, (accumulator, _item) -> accumulator + _item);
        return size;
    }

}
