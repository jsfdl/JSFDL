package com.gitlab.jsfdl.jsfdl.ftp.bulkmode;

import com.gitlab.jsfdl.jsfdl.ftp.download.FTPDownloadFile;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.AuthenticationException;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.DownloadOfflineException;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.ServerOfflineException;

import java.util.List;
import java.util.concurrent.Callable;

public class TraversalRunnerJob implements Callable<List<FTPDownloadFile>> {

    private final TraversalRunner traversalRunner;

    public TraversalRunnerJob(TraversalRunner traversalRunner) {
        this.traversalRunner = traversalRunner;
    }

    @Override
    public List<FTPDownloadFile> call() throws AuthenticationException, ServerOfflineException, DownloadOfflineException {
        return traversalRunner.getFilesToDownload();
    }

}
