package com.gitlab.jsfdl.jsfdl.ftp.client;

import org.apache.commons.net.ftp.FTPFile;

public class FTPFileWithFullPath {

    private final String fullpath;
    private final FTPFile ftpFile;

    public FTPFileWithFullPath(String pathFromStartFolder, FTPFile ftpFile) {
        this.fullpath = pathFromStartFolder;
        this.ftpFile = ftpFile;
    }

    public long getSize() {
        return ftpFile.getSize();
    }

    public String getRemoteFileLocation() {
        return fullpath;
    }


    public boolean isDirectory() {
        return ftpFile.isDirectory();
    }

    public boolean isFile() {
        return ftpFile.isFile();
    }
}
