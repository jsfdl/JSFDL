package com.gitlab.jsfdl.jsfdl.ftp.client;

import com.gitlab.jsfdl.jsfdl.ftp.download.FTPDownloadFile;
import com.gitlab.jsfdl.jsfdl.sfdl.model.ConnectionInfo;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.apache.commons.net.ftp.FTPClient.FTP_SYSTEM_TYPE;

public class FTPConnection {

    private final static Logger logger = LogManager.getLogger();
    private final MyFTPClient ftpClient;
    private final ConnectionInfo connectionInfo;

    public FTPConnection(FTPFactory ftpFactory, ConnectionInfo connectionInfo) {
        logger.trace(() -> "created FTPConnection with ftpClient for " + connectionInfo);
        this.ftpClient = ftpFactory.createFTPClient();
        this.connectionInfo = connectionInfo;
    }

    public void initFTPClient() throws IOException {
        try {
            ftpClient.connect(connectionInfo.getHost(), connectionInfo.getPort());
            ftpClient.setSoTimeout(10000);
            ftpClient.setDefaultTimeout(10000);
            ftpClient.setConnectTimeout(10000);
            ftpClient.enterLocalPassiveMode();
            ftpClient.login(connectionInfo.getUsername(), connectionInfo.getPassword());
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        } catch (IOException e) {
            logger.error("Error initializing ftp client.", e);
            throw e;
        }
    }

    public List<FTPFileWithFullPath> getFilesFromDirectory(String remoteSourceDir) {
        List<FTPFileWithFullPath> files = new ArrayList<>();
        FTPFile[] ftpFiles;
        try {
            if (ftpClient.hasFeature("MLSD"))
                ftpFiles = ftpClient.mlistDir(remoteSourceDir);
            else {
                ftpFiles = ftpClient.listFiles(remoteSourceDir);
            }
        } catch (IOException e) {
            //TODO: move to ftp4j, crapshit ...
            if (e.getMessage().contains("Unable to determine system type")) {
                try {
                    System.setProperty("FTP_SYSTEM_TYPE_DEFAULT", "unix");
                    ftpFiles = ftpClient.listFiles(remoteSourceDir);
                } catch (IOException e1) {
                    throw new RuntimeException("");
                }
            }
            throw new RuntimeException("");
        }

        Arrays.stream(ftpFiles).filter(
                ftpFile -> !ftpFile.getName().equals(".") && !ftpFile.getName().equals(".."))
                .forEach(ftpFile ->
                files.add(new FTPFileWithFullPath(remoteSourceDir.endsWith("/") ? remoteSourceDir + ftpFile.getName() : remoteSourceDir + "/" + ftpFile.getName(), ftpFile)));
        return files;
    }

    private List<FTPFileWithFullPath> listRecursiveRemoteFiles(String remoteSourceDir) throws DirectoryNotAvailableOnFtp {
        logger.trace(() -> "listing files from " + remoteSourceDir);
        try {
            ftpClient.listFiles();
        } catch (Exception ex) {
            //crapshit implementation of ftp. if the system type is not properly set in the ftp server
            //we will get serious problems retrieving FTPFiles. Therefore, if we encounter such a crappy
            //ftp server, we will try to use "UNIX" as default. Maybe i should write some kind of trial-and-error
            //to get the correct system type.
            System.setProperty(FTP_SYSTEM_TYPE, "UNIX");
        }
        try {
            final List<FTPFileWithFullPath> strings = collectFilesFromDirectory(remoteSourceDir);
            logger.trace(() -> "collected files from remote server: " + new ArrayList<>(strings));
            return strings;
        } catch (IOException e) {
            throw new RuntimeException("Unable to collect filelist in directory " + remoteSourceDir);
        }
    }

    private boolean isDirectory(String path) {
        logger.trace(() -> "isDirectory " + path);
        final String pwd;
        try {
            pwd = ftpClient.printWorkingDirectory();
        } catch (IOException e) {
            throw new RuntimeException("unable to get the cwd");
        }
        try {
            //todo check mlst and other extensions, maybe they are better??
            ftpClient.changeWorkingDirectory(path);
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                ftpClient.changeWorkingDirectory(pwd);
            } catch (IOException e) {
                throw new RuntimeException("unable to get the cwd");
            }
        }

    }

    private List<FTPFileWithFullPath> collectFilesFromDirectory(final String folderToScan) throws DirectoryNotAvailableOnFtp, IOException {
        logger.trace(() -> "collectFilesFromDirectory " + folderToScan);
        final List<FTPFileWithFullPath> files = new ArrayList<>();

        //seems like a bug in apaches net ftp impl, i have to cwd to get correct results
        final boolean successful = ftpClient.changeWorkingDirectory(folderToScan);
        if (!successful) {
            logger.error(() -> "unable to change to " + folderToScan);
            throw new DirectoryNotAvailableOnFtp("Error changing to directory");
        }

        final FTPFile[] ftpFiles;
        if (ftpClient.hasFeature("MLSD"))
            ftpFiles = ftpClient.mlistDir();
        else {
            ftpFiles = ftpClient.listFiles();
        }

        for (final FTPFile ftpFile : ftpFiles) {

            if (ftpFile.getName().equals(".") || ftpFile.getName().equals("..")) {
                continue;
            }

            if (ftpFile.isFile()) {
                files.add(new FTPFileWithFullPath(folderToScan.endsWith("/") ? folderToScan + ftpFile.getName() : folderToScan + "/" + ftpFile.getName(), ftpFile));
                continue;
            }

            if (ftpFile.isDirectory()) {
                files.addAll(collectFilesFromDirectory(folderToScan.endsWith("/") ? folderToScan + ftpFile.getName() : folderToScan + "/" + ftpFile.getName()));
                continue;
            }

            if (ftpFile.isSymbolicLink()) {
                if (isDirectory(ftpFile.getName())) {
                    ftpClient.changeWorkingDirectory(ftpFile.getLink());
                    String workingDirectory = ftpClient.printWorkingDirectory();
                    files.addAll(collectFilesFromDirectory(workingDirectory));
                }
            }
        }


        return files;
    }

    public void download(String remoteFile, File localFile) {
        try {
            localFile.getParentFile().mkdirs();
            try (FileOutputStream fileOutputStream = new FileOutputStream(localFile)) {
                boolean result = ftpClient.retrieveFile(remoteFile, fileOutputStream);
                if (!result) {
                    throw new RuntimeException("Error downloading " + remoteFile + " to " + localFile);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void changeToRemoteSourceDir(String remoteSourceDir) throws DirectoryNotAvailableOnFtp {

        boolean success = false;
        try {
            success = ftpClient.changeWorkingDirectory(remoteSourceDir);
        } catch (IOException e) {
            logger.error(e);
        }
        if (!success) {
            throw new DirectoryNotAvailableOnFtp(remoteSourceDir);
        }

    }

    public long getSizeOfFile(String fileOnServer) {
        logger.trace(() -> "getSizeOfFile " + fileOnServer);
        try {
            int ftpStatusCode = ftpClient.sendCommand("SIZE " + fileOnServer);
            if (ftpStatusCode != 213) {
                throw new RuntimeException("Wrong FTP Return - expected 213");
            }
            String rawFtpMessage = ftpClient.getReplyString();
            return Long.parseLong(rawFtpMessage.split(" ")[1].trim());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public void closeConnection() {
        try {
            ftpClient.logout();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void validateFunctionListFiles() {
        ftpClient.validateFunctionListFiles();
    }

    public boolean serverSupportsResume() {
        return ftpClient.serverSupportsResume();
    }

    public long getRemoteFileSize(FTPDownloadFile ftpFile) {
        return ftpClient.getRemoteFileSize(ftpFile);
    }

    public void setRestartOffset(long restartOffset) {
        ftpClient.setRestartOffset(restartOffset);
    }

    public String getStatus(String remoteAbsoluteFileLocation) {
        try {
            return ftpClient.getStatus(remoteAbsoluteFileLocation);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public InputStream retrieveFileStream(String remoteAbsoluteFileLocation) {
        try {
            return ftpClient.retrieveFileStream(remoteAbsoluteFileLocation);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void abort() {
        try {
            ftpClient.abort();
        } catch (IOException e) {

        }
    }

    public void logout() {
        try {
            ftpClient.logout();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendRaw(String feat) {
        try {
            ftpClient.sendCommand(feat);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
