package com.gitlab.jsfdl.jsfdl.ftp;

public enum State {

    WAITING("Waiting"),
    DOWNLOADING("Downloading"),
    FINISHED("Finished"),
    ANALYZING("Analyzing"),
    ANALYZING_FINISHED("Analyzing finished."),
    ABORTED("Aborted"),
    OFFLINE("Offline"),
    AUTHENTICATION("Authentication Error"),
    SERVER_DOWN("Server down."), 
    ERROR("Check logs!"),
    ERROR_PARTIAL_DOWNLOAD("Problem with DL. Check logs."), PARTIAL_DOWNLOAD_FINISHED("Finished (partial).");

    private final String state;

    State(String state) {
        this.state = state;
    }

    public String stringRepres() {
        return state;
    }
}