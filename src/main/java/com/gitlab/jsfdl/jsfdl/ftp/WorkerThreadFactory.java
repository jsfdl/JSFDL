package com.gitlab.jsfdl.jsfdl.ftp;

import java.util.TimerTask;
import java.util.concurrent.ThreadFactory;

public class WorkerThreadFactory implements ThreadFactory {

    private int counter = 0;
    private String prefix = "";

    public WorkerThreadFactory(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public Thread newThread(Runnable r) {
        if (r instanceof TimerTask) {
            return new Thread(r, "timertask-" + counter++);
        }
        return new Thread(r, prefix + "-" + counter++);
    }
}
