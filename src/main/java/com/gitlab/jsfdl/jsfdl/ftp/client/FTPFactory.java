package com.gitlab.jsfdl.jsfdl.ftp.client;


public interface FTPFactory {

    MyFTPClient createFTPClient();

}
