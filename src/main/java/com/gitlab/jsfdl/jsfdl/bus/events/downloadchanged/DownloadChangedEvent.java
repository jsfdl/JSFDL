package com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged;

import com.gitlab.jsfdl.jsfdl.bus.EventWithPayload;

public class DownloadChangedEvent implements EventWithPayload<DownloadInformation> {

    private final DownloadInformation information;

    public DownloadChangedEvent(DownloadInformation information) {
        this.information = information;
    }

    @Override
    public DownloadInformation getPayload() {
        return information;
    }

}
