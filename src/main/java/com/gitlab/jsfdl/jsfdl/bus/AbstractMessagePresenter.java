package com.gitlab.jsfdl.jsfdl.bus;

import java.util.Set;

public abstract class AbstractMessagePresenter {

    /**
     * Registers a list for this presenter for which the admin should inform it.
     *
     * @return a list containing the classes this presenter should get informed
     * about when fired.
     */
    public abstract Set<Class> eventsToListenFor();

    /**
     * This method in the presenter is executed when the event is fired.
     *
     * @param event The event with its payload.
     */
    public abstract void eventFired(Event event);

    public abstract void eventFired(EventWithPayload eventWithPayload);
}
