package com.gitlab.jsfdl.jsfdl.bus;

public interface EventWithPayload<T> extends Event {

    T getPayload();

}
