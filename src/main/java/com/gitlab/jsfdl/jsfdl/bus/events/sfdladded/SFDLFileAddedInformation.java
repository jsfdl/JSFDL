package com.gitlab.jsfdl.jsfdl.bus.events.sfdladded;

import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;

/**
 * Created by oli on 18.02.17.
 */
public class SFDLFileAddedInformation {

    private final SFDLEntry sfdlEntry;
    private final String origName;

    //TODO: converter??
    public SFDLFileAddedInformation(SFDLEntry sfdlEntry, String origName) {
        this.sfdlEntry = sfdlEntry;
        this.origName = origName;
    }

    public int getId() {
        return sfdlEntry.getId();
    }

    public SFDLEntry getSfdlEntry() {
        return sfdlEntry;
    }

}
