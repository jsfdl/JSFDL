package com.gitlab.jsfdl.jsfdl.bus.events.sfdlrequested;

import com.gitlab.jsfdl.jsfdl.bus.EventWithPayload;

public class SFDLRequested implements EventWithPayload<SFDLRequestInfo> {

    private final SFDLRequestInfo sfdlRequestInfo;

    public SFDLRequested(SFDLRequestInfo sfdlRequestInfo) {
        this.sfdlRequestInfo = sfdlRequestInfo;
    }

    @Override
    public SFDLRequestInfo getPayload() {
        return sfdlRequestInfo;
    }
}
