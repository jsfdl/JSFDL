package com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged;

import com.gitlab.jsfdl.jsfdl.ftp.State;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;

import java.time.Instant;

public class DownloadInfoBuilderForSFDL {

    private final SFDLEntry sfdlEntry;
    private long transferedBytes = 0;
    private State state = State.WAITING;
    private Instant startedAt;
    private long secondsRunning = 0;
    private long currentSpeedInBytesPerSecond = 0;
    private long sizeOfPackageInBytes;

    public DownloadInfoBuilderForSFDL(SFDLEntry sfdlEntry) {
        this.sfdlEntry = sfdlEntry;
    }

    public DownloadInfoBuilderForSFDL(DownloadInformation info) {
        this.sfdlEntry = info.getSfdlEntry();
        this.transferedBytes = info.getTransferedBytes();
        this.state = info.getState();
        info.getStartedAt().ifPresent((t) -> this.startedAt = t);
        info.getSecondsRunning().ifPresent((t) -> this.secondsRunning = t);
        this.currentSpeedInBytesPerSecond = info.getBytePerSecond();
        this.sizeOfPackageInBytes = info.getSizeOfPackageInBytes();
    }
    
    

    public DownloadInfoBuilderForSFDL withPackageSize(long sizeOfPackageInBytes) {
        this.sizeOfPackageInBytes = sizeOfPackageInBytes;
        return this;
    }

    public DownloadInfoBuilderForSFDL withTransfered(long bytes) {
        this.transferedBytes = bytes;
        return this;
    }

    public DownloadInfoBuilderForSFDL inState(State state) {
        this.state = state;
        return this;
    }

    public DownloadInfoBuilderForSFDL startedAt(Instant startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    public DownloadInfoBuilderForSFDL runningForSeconds(long secondsRunning) {
        this.secondsRunning = secondsRunning;
        return this;
    }

    public DownloadInfoBuilderForSFDL withSpeed(long currentSpeedInBytesPerSecond) {
        this.currentSpeedInBytesPerSecond = currentSpeedInBytesPerSecond;
        return this;
    }

    public DownloadInformation build() {
        return new DownloadInformation(state, transferedBytes, sfdlEntry, startedAt, secondsRunning, currentSpeedInBytesPerSecond, sizeOfPackageInBytes);
    }

}
