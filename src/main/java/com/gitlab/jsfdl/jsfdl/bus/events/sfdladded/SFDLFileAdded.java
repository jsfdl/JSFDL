package com.gitlab.jsfdl.jsfdl.bus.events.sfdladded;

import com.gitlab.jsfdl.jsfdl.bus.EventWithPayload;

public class SFDLFileAdded implements EventWithPayload<SFDLFileAddedInformation> {

    private final SFDLFileAddedInformation sfdlFile;

    public SFDLFileAdded(SFDLFileAddedInformation sfdlFile) {
        this.sfdlFile = sfdlFile;
    }

    @Override
    public SFDLFileAddedInformation getPayload() {
        return sfdlFile;
    }
}
