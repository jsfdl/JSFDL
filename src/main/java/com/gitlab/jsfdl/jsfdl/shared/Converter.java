package com.gitlab.jsfdl.jsfdl.shared;

public interface Converter<T, T1> {

    T1 convert(T t);

}
