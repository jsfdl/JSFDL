package com.gitlab.jsfdl.jsfdl.update;

import com.gitlab.jsfdl.jsfdl.update.dto.Release;
import com.gitlab.jsfdl.jsfdl.update.dto.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class VersionInfoTest {

    private final VersionFetcher versionFetcher = Mockito.mock(VersionFetcher.class);
    private final static int projectId = 5;

    @Test
    public void check_for_updates_if_tag_list_is_empty_and_expect_no_update() {
        //given
        final int currentVersion = 24;
        when(versionFetcher.loadInfosFromRemote(projectId)).thenReturn(Collections.emptyList());
        VersionInfo sut = new VersionInfo(projectId, currentVersion, versionFetcher);

        //when
        boolean isUpdateAvailable = sut.updateAvailable();

        //then
        assertThat(isUpdateAvailable, is(false));
    }

    @Test
    public void check_for_updates_if_tag_list_contains_elements_expect_no_update() {
        //given
        final int currentVersion = 24;
        final List<Tag> tagList = new ArrayList<>();
        final Tag tag = Mockito.mock(Tag.class);
        when(tag.getTagAsInt()).thenReturn(25);
        tagList.add(tag);
        when(versionFetcher.loadInfosFromRemote(projectId)).thenReturn(tagList);
        VersionInfo sut = new VersionInfo(projectId, currentVersion, versionFetcher);

        //when
        boolean isUpdateAvailable = sut.updateAvailable();

        //then
        assertThat(isUpdateAvailable, is(true));
    }

    @Test
    public void retrieve_update_informations_on_update() {
        //given
        final int currentVersion = 24;
        final List<Tag> tagList = new ArrayList<>();
        final Tag tag = Mockito.mock(Tag.class);
        when(tag.getTagAsInt()).thenReturn(25);
        final Release release = Mockito.mock(Release.class);
        when(tag.getName()).thenReturn("25");
        when(tag.getRelease()).thenReturn(release);
        tagList.add(tag);
        when(versionFetcher.loadInfosFromRemote(projectId)).thenReturn(tagList);
        VersionInfo sut = new VersionInfo(projectId, currentVersion, versionFetcher);

        //when
        final Tag updateInformations = sut.getUpdateInformations();

        //then
        assertThat(updateInformations, is(notNullValue()));
    }
}