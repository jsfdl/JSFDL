package com.gitlab.jsfdl.jsfdl.ftp.download;

import com.gitlab.jsfdl.jsfdl.ftp.client.FTPConnection;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.DownloadOfflineException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junitpioneer.jupiter.TempDirectory;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Path;
import java.util.Scanner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(TempDirectory.class)
public class DownloadRunnableTest {

    private static final String REMOTE_FILE_LOCATION = "/some/long/path/on/ftp/server/12monkeys/sub/sample.xxx";
    private static final String REMOTE_SOURCE_DIRECTORY = "/some/long/path/on/ftp/server/12monkeys";

    private SFDLFileDownloader parent = Mockito.mock(SFDLFileDownloader.class);
    private SFDLEntry sfdlEntry = Mockito.mock(SFDLEntry.class);
    private FTPDownloadFile ftpDownloadFile = Mockito.mock(FTPDownloadFile.class);
    private FTPConnection ftpConnection = Mockito.mock(FTPConnection.class);
    private ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(new byte[]{65, 65, 65, 65, 65, 65});

    @BeforeEach
    public void setUp() {
        when(ftpDownloadFile.getRemoteAbsoluteFileLocation()).thenReturn(REMOTE_FILE_LOCATION);
        when(ftpDownloadFile.getSourceDirectoryOnServer()).thenReturn(REMOTE_SOURCE_DIRECTORY);
    }

    @Test
    public void cant_download_nonexistent_file(@TempDirectory.TempDir Path temporaryFolder) {
        when(ftpConnection.getStatus(any())).thenThrow(new DownloadOfflineException("blah"));
        DownloadRunnable sut = new DownloadRunnable(parent, sfdlEntry, temporaryFolder.getRoot().toAbsolutePath().toString(),
                ftpDownloadFile, ftpConnection);
         assertThrows(DownloadOfflineException.class, sut::call);

    }

    @Test
    public void does_create_folder_when_start_download(@TempDirectory.TempDir Path temporaryFolder) throws InterruptedException {

        when(ftpConnection.retrieveFileStream(REMOTE_FILE_LOCATION)).thenReturn(byteArrayInputStream);
        DownloadRunnable sut = new DownloadRunnable(parent, sfdlEntry, temporaryFolder.getRoot().toAbsolutePath().toString(),
                ftpDownloadFile, ftpConnection);
        sut.call();
        assertThat(new File(temporaryFolder.getRoot().toFile(), "/sub").exists(), is(true));
        assertThat(new File(temporaryFolder.getRoot().toFile(), "/sub").isDirectory(), is(true));
        assertThat(new File(temporaryFolder.getRoot().toFile(), "/sub/sample.xxx").isFile(), is(true));
    }

    @Test
    public void does_overwrite_when_destinationfile_exists_but_server_doesnt_support_resume(@TempDirectory.TempDir Path temporaryFolder) throws Exception {
        //given
        when(ftpConnection.retrieveFileStream(REMOTE_FILE_LOCATION)).thenReturn(byteArrayInputStream);
        File alreadyExistentFile = new File(temporaryFolder.getRoot().toFile(), "/sub/sample.xxx");
        alreadyExistentFile.getParentFile().mkdirs();
        try (FileWriter writer = new FileWriter(alreadyExistentFile)) {
            writer.write("hello world");
        }

        //when
        DownloadRunnable sut = new DownloadRunnable(parent, sfdlEntry, temporaryFolder.getRoot().toAbsolutePath().toString(),
                ftpDownloadFile, ftpConnection);
        sut.call();

        //then
        assertThat(new File(temporaryFolder.getRoot().toFile(), "/sub").exists(), is(true));
        assertThat(new File(temporaryFolder.getRoot().toFile(), "/sub").isDirectory(), is(true));
        assertThat(new File(temporaryFolder.getRoot().toFile(), "/sub/sample.xxx").isFile(), is(true));
        Scanner scanner = new Scanner(new File(temporaryFolder.getRoot().toFile(), "/sub/sample.xxx"));
        assertThat(scanner.nextLine().contains("hello world"), is(false));
    }

    @Test
    public void does_resume_when_destinationfile_exists_and_resume_possible(@TempDirectory.TempDir Path temporaryFolder) throws Exception {
        //given
        when(ftpConnection.retrieveFileStream(REMOTE_FILE_LOCATION)).thenReturn(byteArrayInputStream);
        when(ftpConnection.serverSupportsResume()).thenReturn(true);
        when(ftpConnection.getRemoteFileSize(any())).thenReturn(20L);
        File alreadyExistentFile = new File(temporaryFolder.getRoot().toFile(), "/sub/sample.xxx");
        alreadyExistentFile.getParentFile().mkdirs();
        try (FileWriter writer = new FileWriter(alreadyExistentFile)) {
            writer.write("hello world");
        }
        DownloadRunnable sut = new DownloadRunnable(parent, sfdlEntry, temporaryFolder.getRoot().toAbsolutePath().toString(),
                ftpDownloadFile, ftpConnection);
        //when
        sut.call();

        //then
        assertThat(new File(temporaryFolder.getRoot().toFile(), "/sub").exists(), is(true));
        assertThat(new File(temporaryFolder.getRoot().toFile(), "/sub").isDirectory(), is(true));
        assertThat(new File(temporaryFolder.getRoot().toFile(), "/sub/sample.xxx").isFile(), is(true));
        Scanner scanner = new Scanner(new File(temporaryFolder.getRoot().toFile(), "/sub/sample.xxx"));
        verify(ftpConnection).setRestartOffset(11);
        assertThat(scanner.nextLine().contains("hello worldAAAAAA"), is(true));
    }

    //TODO: here is a bug: we dont need a server supporting resume to check if the
    //TODO: downloaded file is already fully transfered! refactor..
    @Test
    public void does_skip_download_on_resume_but_file_got_transfered_completly(@TempDirectory.TempDir Path temporaryFolder) throws Exception {
        //given
        when(ftpConnection.retrieveFileStream(REMOTE_FILE_LOCATION)).thenReturn(byteArrayInputStream);
        when(ftpConnection.getSizeOfFile(REMOTE_FILE_LOCATION)).thenReturn(11L);
        when(ftpConnection.serverSupportsResume()).thenReturn(true); //remove that line after bugfix
        File alreadyExistentFile = new File(temporaryFolder.getRoot().toFile(), "/sub/sample.xxx");
        alreadyExistentFile.getParentFile().mkdirs();
        try (FileWriter writer = new FileWriter(alreadyExistentFile)) {
            writer.write("hello world");
        }
        DownloadRunnable sut = new DownloadRunnable(parent, sfdlEntry, temporaryFolder.getRoot().toAbsolutePath().toString(),
                ftpDownloadFile, ftpConnection);
        //when
        sut.call();
        //then
        verify(ftpConnection, times(0)).retrieveFileStream(any());
    }
}