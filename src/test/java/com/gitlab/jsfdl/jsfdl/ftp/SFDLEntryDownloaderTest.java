package com.gitlab.jsfdl.jsfdl.ftp;

import com.gitlab.jsfdl.jsfdl.app.JSFDLConfig;
import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadInformation;
import com.gitlab.jsfdl.jsfdl.ftp.client.FTPConnection;
import com.gitlab.jsfdl.jsfdl.ftp.client.FTPFactory;
import com.gitlab.jsfdl.jsfdl.ftp.client.MyFTPClient;
import com.gitlab.jsfdl.jsfdl.ftp.download.FTPConnectionPool;
import com.gitlab.jsfdl.jsfdl.ftp.download.SFDLFileDownloader;
import com.gitlab.jsfdl.jsfdl.ftp.listmode.FTPValidator;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;
import com.gitlab.jsfdl.jsfdl.sfdl.model.ConnectionInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlConnectionInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlFileInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlPackages;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlSFDLPackage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

class SFDLEntryDownloaderTest {

    private SFDLEntry sfdlEntry = mock(SFDLEntry.class);
    private XmlPackages xmlPackages = mock(XmlPackages.class);
    private XmlSFDLPackage xmlSfdlPackage = mock(XmlSFDLPackage.class);
    private FTPValidator ftpValidator = mock(FTPValidator.class);
    private FTPFactory ftpFactory = mock(FTPFactory.class);
    private List<XmlFileInfo> xmlFileInfoList = new ArrayList<>();
    private XmlFileInfo xmlFileInfo = mock(XmlFileInfo.class);
    private XmlConnectionInfo xmlConnectionInfo = mock(XmlConnectionInfo.class);
    private JSFDLConfig sfdlConfig = mock(JSFDLConfig.class);
    private MyFTPClient myFTPClient = mock(MyFTPClient.class);
    private FTPConnection ftpConnection = mock(FTPConnection.class);
    private FTPConnectionPool ftpConnectionPoolMock = mock(FTPConnectionPool.class);

    @BeforeEach
    void setUp() {
        when(ftpConnectionPoolMock.getFreeConnection()).thenReturn(ftpConnection);
    }

    @Test
    void can_start_a_download_with_listmode() {
        //given
        when(xmlFileInfo.getFileSize()).thenReturn(500L);
        xmlFileInfoList.add(xmlFileInfo);
//        when(sfdlFile.getConnectionInfo()).thenReturn(connectionInfo);
//        when(sfdlFile.getPackages()).thenReturn(packages);
        when(xmlSfdlPackage.getXmlFileInfo()).thenReturn(xmlFileInfoList);
        when(xmlPackages.getPackages()).thenReturn(Collections.singletonList(xmlSfdlPackage));
        when(ftpFactory.createFTPClient()).thenReturn(myFTPClient);
        when(xmlConnectionInfo.getHost()).thenReturn("localhorst");

        SFDLFileDownloader sut = new SFDLFileDownloader(sfdlEntry, sfdlConfig, ftpValidator, ftpFactory);
        //when
        sut.download();

        //then
        assertThat(sut.getState(), is(State.FINISHED));
        assertThat(sut.getSfdlEntry(), is(sfdlEntry));
    }

    @Test
    void can_start_a_download_with_bulkmode() {
        //given
        when(xmlFileInfo.getFileSize()).thenReturn(500L);
        xmlFileInfoList.add(xmlFileInfo);
        when(sfdlEntry.isBulkfolder()).thenReturn(true);
        when(xmlSfdlPackage.getXmlFileInfo()).thenReturn(xmlFileInfoList);
        when(xmlPackages.getPackages()).thenReturn(Collections.singletonList(xmlSfdlPackage));
        when(ftpFactory.createFTPClient()).thenReturn(myFTPClient);
        when(xmlConnectionInfo.getHost()).thenReturn("localhorst");
        when(sfdlEntry.getBulkRemoteSourceDir()).thenReturn("/someRandomPackage/");

        SFDLFileDownloader sut = new SFDLFileDownloader(sfdlEntry, sfdlConfig, ftpValidator, ftpFactory) {
            @Override
            protected FTPConnectionPool getFtpConnectionPool(int maxThreads, FTPFactory ftpFactory, ConnectionInfo connectionInfo) {
                return ftpConnectionPoolMock;
            }
        };
        //when
        sut.download();

        //then
        assertThat(sut.getState(), is(State.FINISHED));
        assertThat(sut.getSfdlEntry(), is(sfdlEntry));
    }

    @Test
    void can_not_start_a_download_when_validation_errors_occured() {
        //given
        when(xmlFileInfo.getFileSize()).thenReturn(500L);
        xmlFileInfoList.add(xmlFileInfo);
        when(ftpValidator.validate(ArgumentMatchers.isA(SFDLEntry.class))).thenReturn(Arrays.asList(FTPValidator.VALIDATION_STATE.FILES_OFFLINE));
        when(xmlSfdlPackage.getXmlFileInfo()).thenReturn(xmlFileInfoList);
        when(xmlPackages.getPackages()).thenReturn(Collections.singletonList(xmlSfdlPackage));
        when(ftpFactory.createFTPClient()).thenReturn(myFTPClient);
        when(xmlConnectionInfo.getHost()).thenReturn("localhorst");
        DownloadsMvp.DownloadChangedListener listener = mock(DownloadsMvp.DownloadChangedListener.class);

        SFDLFileDownloader sut = new SFDLFileDownloader(sfdlEntry, sfdlConfig, ftpValidator, ftpFactory) {
            @Override
            protected FTPConnectionPool getFtpConnectionPool(int maxThreads, FTPFactory ftpFactory, ConnectionInfo connectionInfo) {
                return ftpConnectionPoolMock;
            }
        };
        sut.addDownloadChangedListener(listener);
        //when
        assertThrows(RuntimeException.class, sut::download);

        //then
        assertThat(sut.getState(), is(State.OFFLINE));
    }

    @Test
    void can_register_a_listener() {
        //given
        SFDLFileDownloader sut = new SFDLFileDownloader(sfdlEntry, sfdlConfig, ftpValidator, ftpFactory);
        DownloadsMvp.DownloadChangedListener listener = mock(DownloadsMvp.DownloadChangedListener.class);
        sut.addDownloadChangedListener(listener);
        sut.addDownloadChangedListener(listener);

        //when
        sut.download();
        //then
        verify(listener).downloadChanged(isA(DownloadInformation.class));

    }
}