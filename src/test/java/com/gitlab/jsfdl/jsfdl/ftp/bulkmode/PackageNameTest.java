package com.gitlab.jsfdl.jsfdl.ftp.bulkmode;


import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PackageNameTest {

    @Test
    public void removes_starting_slash() {
        //given
        final PackageName sut = new PackageName("/test");
        //when
        final String packagenameFromSourcedir = sut.getPackagenameFromSourcedir();

        //then
        assertThat(packagenameFromSourcedir, is("test"));
    }

    @Test
    public void removes_nothing_when_naming_is_ok() {
        //given
        final PackageName sut = new PackageName("test");
        //when
        final String packagenameFromSourcedir = sut.getPackagenameFromSourcedir();

        //then
        assertThat(packagenameFromSourcedir, is("test"));
    }

    @Test
    public void removes_parts_before_slash() {
        //given
        final PackageName sut = new PackageName("test/test");
        //when
        final String packagenameFromSourcedir = sut.getPackagenameFromSourcedir();

        //then
        assertThat(packagenameFromSourcedir, is("test"));
    }

    @Test
    public void removes_trailing_slash() {
        //given
        final PackageName sut = new PackageName("test/");
        //when
        final String packagenameFromSourcedir = sut.getPackagenameFromSourcedir();

        //then
        assertThat(packagenameFromSourcedir, is("test"));
    }

    @Test
    public void removes_starting_and_trailing_slash() {
        //given
        final PackageName sut = new PackageName("/test/");
        //when
        final String packagenameFromSourcedir = sut.getPackagenameFromSourcedir();

        //then
        assertThat(packagenameFromSourcedir, is("test"));
    }
}