package com.gitlab.jsfdl.jsfdl.app;

import com.gitlab.jsfdl.jsfdl.ftp.download.SFDLDownloaderThreadPoolExecutor;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;
import com.gitlab.jsfdl.jsfdl.sfdl.SFDLEntryTest;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLFile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author oli
 */
public class JSFDLEngineWorkPoolTest {

    private SFDLDownloaderThreadPoolExecutor workPool;
    private SFDLFile sfdlFile;
    private DownloadsMvp.DownloadChangedListener listener;

    @BeforeEach
    public void setUp() throws Exception {
        workPool = new SFDLDownloaderThreadPoolExecutor(50);
        byte[] readAllBytes = Files.readAllBytes(Paths.get(SFDLEntryTest.class.getResource("12monkeys_sfdlv6.xml").toURI()));
//        sfdlFile = SFDLFile.getSfdlFile(readAllBytes, SFDLFileTest.class.getResource("12monkeys_sfdlv6.xml").getFile());
//        listener = Mockito.mock(DownloadsMvp.DownloadChangedListener.class);
    }

    @Test
    public void registerADownload() {
//        FTPValidator ftpValidator = Mockito.mock(FTPValidator.class);
//        FTPFactory factory = Mockito.mock(FTPFactory.class);
//        workPool.register(new SFDLFileDownloader(sfdlFile, new JSFDLConfig("destination", "sfdls"), ftpValidator, factory));
//        Set<SFDLFileDownloader> registeredJobs = workPool.getRegisteredTasks();
//        assertThat(registeredJobs.size(), is(1));
    }

    @Test
    public void startADownload() {
//        workPool.register(sfdlFile);
//        workPool.startDownloadFor(sfdlFile);
//        final List<SFDLFileDownloader> runningDownloads = workPool.getRunningDownloads();
//        assertThat(runningDownloads.size(), is(1));
    }
}
