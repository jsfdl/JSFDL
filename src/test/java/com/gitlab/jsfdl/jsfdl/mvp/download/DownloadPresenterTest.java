package com.gitlab.jsfdl.jsfdl.mvp.download;

import com.gitlab.jsfdl.jsfdl.bus.events.abortalldownloads.AbortAllDownloads;
import com.gitlab.jsfdl.jsfdl.bus.events.startalldownloads.StartAllDownloads;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp.Model;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Set;

import static com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp.View;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DownloadPresenterTest {

    Model mainModel;
    View mainView;
    DownloadPresenter presenter;

    @BeforeEach
    public void setUp() {
        mainModel = Mockito.mock(Model.class);
        mainView = Mockito.mock(View.class);
        presenter = new DownloadPresenter(mainView, mainModel);
    }

    @Test
    public void validateWeListenForDownloadChanges() {
        final Set<Class> classes = presenter.eventsToListenFor();
        assertThat(classes.contains(AbortAllDownloads.class), is(true));
        assertThat(classes.contains(StartAllDownloads.class), is(true));
    }

    @Test
    public void stateOfADownloadChanged() {
//        final SFDLFile sfdlFile = new SFDLFile(new File(SFDLFileTest.class.getResource("12monkeys.xml").getFile()));
//        final DownloadChangedEvent downloadChangedEvent = new DownloadChangedEvent(new DownloadInfoBuilderForSFDL(sfdlFile).build());
//        presenter.eventFired(downloadChangedEvent);
//        verify(mainModel).downloadChangedFor(downloadChangedEvent);
//        verify(mainView).updateDownload(downloadChangedEvent);
    }
}
