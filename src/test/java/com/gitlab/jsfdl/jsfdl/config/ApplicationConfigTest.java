package com.gitlab.jsfdl.jsfdl.config;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;

public class ApplicationConfigTest {

    @Test
    public void can_load_and_interpret_version_information() {
        //given
        ApplicationConfig config = new ApplicationConfig();

        //when
        final int currentVersion = config.getCurrentVersion();

        //then
        assertThat(currentVersion, instanceOf(Integer.class));
    }
}